var flexo = function() {
	var self = {
		DS: '/'
	};

	self.loadMethods = function(o, classDir, className, methods, customMethods) {
		if(methods) {
			var methodName, methodPath;
			for(var m in methods) {
				methodName = methods[m];
				methodPath = self.getMethodPath(classDir, className, methodName, customMethods);
				o[methodName] = require(methodPath)(o);
			}
		}
	};

	self.getMethodPath = function(classDir, className, methodName, customMethods) {
		var methodFile = 'default';
		if(typeof(customMethods[methodName])!='undefined') {
			methodFile = customMethods[methodName];
		}
		var methodPath = classDir + methodName + self.DS + methodFile;
		return methodPath;
	};

	self.getClassDirFromPath = function(classPath) {
		var index = classPath.lastIndexOf(self.DS) + 1;
		return classPath.substring(0, index);
	};

	self.getClassNameFromPath = function(classPath) {
		var index = classPath.lastIndexOf(self.DS) + 1;
		return classPath.substr(index).replace('.js', '');
	};

	self.interface = {};

	self.interface.init = function(classPath, customOptions, extendPath) {
		var options = {
			methods: [],
			customMethods: {}
		};
		if(typeof(customOptions)=='object') {
			for(var opt in customOptions) {
				options[opt] = customOptions[opt];
			}
		}
		// TODO: methods um Einträge in customMethods erweitern
		// TODO: customMethods (wie auch immer) durchreichen an extendPath, damit da auch das Richtige geladen wird
		// TODO: Sicherstellen dass Methoden nicht mehrfach geladen werden, womöglich aus der gleichen Datei
		var o = {};
		// console.log(classPath+' flexo.init');
		if(extendPath) {
			// console.log(classPath+'    flexo.extendPath '+extendPath);
			o = new require(extendPath)();
			// if(typeof(o)=='function') {
			// 	o = o();
			// }
		}
		var classDir = self.getClassDirFromPath(classPath);
		var className = self.getClassNameFromPath(classPath);
		self.loadMethods(o, classDir, className, options.methods, options.customMethods);
		return o;
	};

	return self.interface;
};

module.exports = flexo;
